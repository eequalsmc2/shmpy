#pragma once

#include <cstring>
#include <stdexcept>

struct ShmError : public std::runtime_error {
  ShmError(const std::string &msg) : runtime_error(msg) {}
  ShmError(int err) : runtime_error(strerror(err)) {}
};

struct HandleError : public std::runtime_error {
  HandleError(std::string &&msg) : runtime_error(msg) {}
};

struct VariableError : public std::runtime_error {
  VariableError(std::string &&msg) : runtime_error(msg) {}
};