#pragma once
#include "spdlog/common.h"
#include <mutex>
#include <pthread.h>
#include <pybind11/pybind11.h>
#include <pybind11/pytypes.h>

// mutex attribute
inline static pthread_mutexattr_t PTHREAD_MUTEXATTR;
inline static std::once_flag OC;
inline void init_mutex_attr() {
  std::call_once(OC, [] {
    pthread_mutexattr_init(&PTHREAD_MUTEXATTR);
    pthread_mutexattr_setpshared(&PTHREAD_MUTEXATTR, PTHREAD_PROCESS_SHARED);
  });
}

// logfile
inline static std::string LOGFILE = "__pycache__/shmpy.log";
inline void set_logfile(std::string &&path) { LOGFILE = path; }
inline static spdlog::level::level_enum LOG_LEVEL =
    spdlog::level::level_enum::debug;
inline void set_loglevel(spdlog::level::level_enum level) { LOG_LEVEL = level; }
