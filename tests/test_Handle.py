from os import path
import unittest
import numpy as np
import shmpy


class TestShmpyHandle(unittest.TestCase):
    def test_create(self):
        name = "test_handle1"
        capacity = 128

        handle = shmpy.Handle(name, capacity)
        self.assertTrue(path.exists(f"/dev/shm/{name}"))
        self.assertEqual(handle.name, name)
        self.assertEqual(handle.size, 0)
        self.assertEqual(handle.ref_count, 1)
        self.assertEqual(handle.capacity, capacity)
        del handle
        self.assertFalse(path.exists(f"/dev/shm/{name}"))

    def test_attach_1(self):
        name = "test_handle2"
        capacity = 128

        handle1 = shmpy.Handle(name, capacity)
        handle2 = shmpy.Handle(name)
        self.assertTrue(path.exists(f"/dev/shm/{name}"))
        self.assertEqual(handle2.name, name)
        self.assertEqual(handle2.size, 0)
        self.assertEqual(handle1.ref_count, 2)
        self.assertEqual(handle2.ref_count, 2)
        self.assertEqual(handle2.capacity, capacity)
        del handle1
        self.assertTrue(path.exists(f"/dev/shm/{name}"))
        self.assertEqual(handle2.ref_count, 1)
        del handle2
        self.assertFalse(path.exists(f"/dev/shm/{name}"))

    def test_attach_2(self):
        name = "test_handle3"
        capacity = 128

        handle1 = shmpy.Handle(name, capacity)
        handle2 = shmpy.Handle(name)
        self.assertTrue(path.exists(f"/dev/shm/{name}"))
        self.assertEqual(handle2.name, name)
        self.assertEqual(handle2.size, 0)
        self.assertEqual(handle1.ref_count, 2)
        self.assertEqual(handle2.ref_count, 2)
        self.assertEqual(handle2.capacity, capacity)
        del handle2
        self.assertTrue(path.exists(f"/dev/shm/{name}"))
        self.assertEqual(handle1.ref_count, 1)
        del handle1
        self.assertFalse(path.exists(f"/dev/shm/{name}"))

    def test_insert_ndarray_1(self):
        name = "test_handle4"
        capacity = 8
        handle = shmpy.Handle(name, capacity)
        self.assertEqual(handle.size, 0)
        arr = np.random.randn(64, 64)
        handle.insert("arr", arr)
        self.assertEqual(handle.size, 1)

    def test_insert_ndarray_2(self):
        name = "test_handle5"
        capacity = 8
        handle = shmpy.Handle(name, capacity)
        self.assertEqual(handle.size, 0)
        for i in range(capacity):
            handle.insert(f"arr_{i}", np.random.randn(64, 64))
            self.assertEqual(handle.size, i+1)

    def test_get_ndarray_1(self):
        name = "test_handle6"
        capacity = 8
        handle = shmpy.Handle(name, capacity)
        arr = np.random.randn(64, 64)
        handle.insert("arr", arr)
        shmarr = handle.get("arr")
        self.assertEqual(arr.sum(), shmarr.sum())

    def test_get_ndarray_2(self):
        name = "test_handle7"
        capacity = 8
        handle = shmpy.Handle(name, capacity)
        # generate ndarray
        arr_list = [np.random.randn(10, 10) for _ in range(capacity)]
        for (i, arr) in enumerate(arr_list):
            handle.insert(f"arr_{i}", arr)
        self.assertEqual(handle.size, capacity)
        for (i, arr) in enumerate(arr_list):
            self.assertEqual(arr.sum(), handle.get(f"arr_{i}").sum())

    def test_insert_str_1(self):
        name = "test_handle8"
        capacity = 16
        handle = shmpy.Handle(name, capacity)
        self.assertEqual(handle.size, 0)
        handle.insert("mystr", "hello, world!")
        self.assertEqual(handle.size, 1)

    def test_insert_str_2(self):
        name = "test_handle9"
        capacity = 16
        handle = shmpy.Handle(name, capacity)
        self.assertEqual(handle.size, 0)
        for i in range(capacity):
            handle.insert(f"mystr_{i}", f"[{i}] hello world!")
            self.assertEqual(handle.size, i+1)

    def test_get_str_1(self):
        name = "test_handle10"
        capacity = 16
        insertStr = "hello world!!!"

        handle = shmpy.Handle(name, capacity)
        handle.insert("mystr", insertStr)
        self.assertEqual(insertStr, handle.get("mystr"))

    def test_get_str_2(self):
        name = "test_handle11"
        capacity = 16
        baseStr = "hello world!!!"
        handle = shmpy.Handle(name, capacity)
        for i in range(capacity):
            handle.insert(f"mystr_{i}", f"[{i}]: {baseStr}")
            self.assertEqual(handle.size, i+1)
        for i in range(capacity):
            self.assertEqual(handle.get(f"mystr_{i}"), f"[{i}]: {baseStr}")
