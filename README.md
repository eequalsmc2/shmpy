# shmpy - Share data between process via IPC(POSIX API)

**warning**  
This is just an early version. Only test on Linux 5.4 x86_64 Python3.8. Win32 version will be provided later.


## Core feature
1. share data via IPC(Shared Memory)
2. Specific dtypes e.g. *primitives* *pylist*, *pydict*, *pystr*, *pytuple* and *py buffer protocol* can shared between difference language.
3. fast

## Build
```
cd shmpy
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=MinSizeRel ..
make -j
```
after that, you should see a shared lib with similar name as `shmpy.cpython-38-x86_64-linux-gnu.so` (name might be different, depend on with version python lib you are linking).

## Usage

### Quick Start
```py
# process 1
import numpy as np
import shmpy
import os

# create an np.ndarray
arr = np.random.randn(512, 512)
print(f"{os.getpid()} - arrsum: {arr.sum()}")
print(f"{os.getpid()} - arrshape: {arr.shape}")

# create shmpy handle with 128 variable capacity
handle = shmpy.Handle("handle1", 128)
# insert arr into the shared memory
handle.insert("myarr", arr)
```

```py
# process 2
import numpy as np
import shmpy
import os

print(os.getpid())

# attach to exising shmpy handle called "handle1"
handle = shmpy.Handle("handle1")
# get arr from the shared memory
arr = handle.get("myarr")
print(f"{os.getpid()} - arrshape: {arr.shape}")
print(f"{os.getpid()} - arrsum: {arr.sum()}")
```

### Customize Logpath
```py
import shmpy

handle1 = shmpy.Handle("handle1", 8)

# print default logfile path __pycache__/shmpy.log
print(shmpy.get_logfile())
# modify logfile path
shmpy.set_logfile("./shmpy.log")

# now handle1's logfile is still "__pycache__/shmpy.log"
# if a new handle is created, its logfile path will be ./shmpy.log
```